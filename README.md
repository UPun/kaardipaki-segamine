"# My project's README"

JavaScripti harjutamiseks koostasin programmi, mis loob massiivi 'kaardipakk', mis koosneb 36 kaardist. 
Klikates nupule 'Kaardipaki segamine', segatakse 'kaardipakk' juhuslikult ringi tõstes kahte juhuslikku kaarti 50 korda. 
(50 on piisav, et kaardipakk totaalselt segi pöörata ja see toimub väga kiiresti).
Head katsetamist!

""# My project's README"

The JavaScript program designed for practicing, which creates an array of the playing cards, which consists of 36 cards. 
By clicking on the button 'Kaardipaki segamine', a mix of playing cards in random order will happen around the raising of two random cards, 50 times. 
50 is enough to make a complete set of cards totally confused and it's happening very quickly.

https://en.wikipedia.org/wiki/Playing_card

A complete set of cards is called a pack (UK English), deck (US English), or set (Universal), looks like before mixing in such a shape:
["ri6 ", "ri7 ", "ri8 ", "ri9 ", "ri10 ", "riP ", "riE ", "riK ", "riÄ ",
 "ru6 ", "ru7 ", "ru8 ", "ru9 ", "ru10 ", "ruP ", "ruE ", "ruK ", "ruÄ ",
 "po6 ", "po7 ", "po8 ", "po9 ", "po10 ", "poP ", "poE ", "poK ", "poÄ ",
 "är6 ", "är7 ", "är8 ", "är9 ", "är10 ", "ärP ", "ärE ", "ärK ", "ärÄ "]

Chrome, Firefox, Safari, Opera, Internet Explorer are supported.

A happy testing!